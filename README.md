# pythonRename

This is a simple python app to rename a batch of files in a folder.

It assumes you are on linux or MAC and have python3 in /usr/bin/python3

If you don't you'll want to adjust the shebang.