#!/usr/bin/python3

import os

print("!!Warning:  This program will apply this change to anything in the directory!!\n")
print("!!This program will change file extensions!!\n")
oldPattern = input("Input the text you want to replace in the filename: ")
newPattern = input("Input the text you want to replace it with: ")

def main():
    for fileName in os.listdir("."):
        os.rename(fileName, fileName.replace(oldPattern,newPattern))

if __name__=='__main__':
    main()

